﻿

#include <iostream>

class Animal {
public:
	virtual void Voice()
	{
		std::cout << "Voice" << std::endl;
	}
};
class Dog : public Animal
{
public:
	
	void Voice() override
	{
		std::cout << "Woof!" << std::endl;
	}
};
class Sheep : public Animal
{
public:

	void Voice() override
	{
		std::cout << "Bee!" << std::endl;
	}
};
class Cat : public Animal {
public:
	void Voice() override
	{
		std::cout << "Meow!" << std::endl;
	}

};

int main()

{
	int arrSize = 12;



	Animal** arr = new Animal*[arrSize];

	for (int i = 0; i < arrSize; i++) {
		if (i%2) {
			arr[i] = new Dog();
		}
		else if (i%3) {
			arr[i] = new Sheep();
		}
		else {
			arr[i] = new Cat();
		}
	}
	for (int i = 0; i < arrSize; i++) {
		arr[i]->Voice();

	}
	delete[] arr;

}

